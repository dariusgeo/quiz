package com.geodani.sda.quiz.controller;

import java.util.Iterator;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.SessionAttribute;
import org.springframework.web.servlet.ModelAndView;

import com.geodani.sda.quiz.model.GivenAnswer;
import com.geodani.sda.quiz.model.Question;
import com.geodani.sda.quiz.model.Quiz;
import com.geodani.sda.quiz.service.GivenAnswerService;
import com.geodani.sda.quiz.service.QuestionService;
import com.geodani.sda.quiz.service.ResultService;

@Controller
public class QuestionController {
	
	@Autowired
	private QuestionService questionService;
	
	@Autowired
	private GivenAnswerService givenAnswerService;
	
	@Autowired
	private ResultService resultService;

	@PostMapping("/nextQuestion")
	public ModelAndView authenticateUser(@SessionAttribute("quiz") Quiz quiz, @ModelAttribute("question") Question question, ModelMap model, HttpServletRequest request) {

		try {
			
			String actionParam = request.getParameter("action");	
			String questionId = request.getParameter("questionId");
			String select[] = request.getParameterValues("answer");
							
			if (select != null && select.length != 0) {
			   System.out.println("You have selected: ");
				for (int i = 0; i < select.length; i++) {
				  System.out.println(select[i]); 
				  //TODO Check if record already exists, if not then create it
				  GivenAnswer existingGivenAnswer = givenAnswerService.getByQuizIdAndQuestionId(quiz.getId(), Long.parseLong(questionId));
				  if (null == existingGivenAnswer) {
					  givenAnswerService.create(quiz.getId(), Long.parseLong(questionId), select[i]);
				  } else {
					  givenAnswerService.update(quiz.getId(), Long.parseLong(questionId), select[i]);
				  }
				 
				}
			}
						
			long lastQuestionId = quiz.getQuestions().size();	
			Question q = null;
			if("Finish Exam".equals(actionParam)) {   
				// actionParam == Finish Exam 
				// calculate score
				// redirect to score page
				
				double result = resultService.calculateResult(quiz);				
				model.put("result", result);
				//request.getSession().invalidate();
				
				return new ModelAndView("result", model);
			} else if ("Next".equals(actionParam)) {
				q = questionService.getById(lastQuestionId + 1);
				quiz.getQuestions().add(q);
			} else if ("Previous".equals(actionParam)) {
				Question lastQuestion = questionService.getById(lastQuestionId);	
				Iterator<Question> it = quiz.getQuestions().iterator();
				while (it.hasNext()) {
					Question quest = (Question)it.next();
					if (quest.getId() == lastQuestion.getId()) {
						it.remove();
					}
				}
				//quiz.getQuestions().remove(lastQuestion);
				q = questionService.getById(lastQuestionId - 1);		
			}
			 
		    model.put("question", q);
			model.put("quiz", quiz);
		
		} catch (Exception ex ){
			ex.printStackTrace();
		}

		return new ModelAndView("question", model);
	}
		
//	@ModelAttribute("question")
//	public Question getById(Long id) {
//		return questionService.getById(id);
//	}
}
