package com.geodani.sda.quiz.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.servlet.ModelAndView;

import com.geodani.sda.quiz.exception.NotAuthorizedException;
import com.geodani.sda.quiz.model.User;
import com.geodani.sda.quiz.service.UserService;

@Controller
@SessionAttributes("user")
public class LoginController {
	
	
	@Autowired
	private UserService userService;
	
	@ModelAttribute("user")
    public User setUpUserForm() {
      return new User();
    }
	 
	@RequestMapping(value = {"/", "/home"}, method = { RequestMethod.GET, RequestMethod.POST })
    public String index() {
      return "login";
    }
	
	@PostMapping("/login")
	public ModelAndView authenticateUser(@ModelAttribute("user") User user,  ModelMap model) throws NotAuthorizedException {
				
		User dbUser = userService.getByEmail(user.getEmail());

		if (null == dbUser || false == dbUser.getPassword().equals(user.getPassword())) {
			throw new NotAuthorizedException("User not authorized!");
		}
				
	
		model.put("user", dbUser);
		
		return new ModelAndView("loginSuccessful", model);
	}
}