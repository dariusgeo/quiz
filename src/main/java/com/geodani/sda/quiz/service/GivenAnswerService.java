package com.geodani.sda.quiz.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.geodani.sda.quiz.model.Answer;
import com.geodani.sda.quiz.model.GivenAnswer;
import com.geodani.sda.quiz.model.Question;
import com.geodani.sda.quiz.model.Quiz;
import com.geodani.sda.quiz.respository.GivenAnswerRepository;

@Service
@Transactional
public class GivenAnswerService {

	@Autowired
	private GivenAnswerRepository repository;
	
	@Autowired
	private QuestionService questionService;
	
	@Autowired
	private AnswerService answerService;
	
	@Autowired
	private QuizService quizService;
	
	
	public List<GivenAnswer> getAllByQuizId(Long id){
		
		return repository.getAllByQuizId(id);
	}
	
   public GivenAnswer getByQuizIdAndQuestionId(Long quizId, Long questionId){
		
		return repository.getOneByQuizIdAndQuestionId(quizId, questionId);
	}
   
   public GivenAnswer getOneByQuizIdAndQuestionIdAndAnswerId(Long quizId, Long questionId, Long answerId){
		
		return repository.getOneByQuizIdAndQuestionIdAndAnswerId(quizId, questionId, answerId);
	}

	public void create(Long quizId, Long questionId, String answerId) {
		GivenAnswer givenAnswer = new GivenAnswer();
		Quiz quiz = quizService.getById(quizId);
		Question question = questionService.getById(questionId);
		Answer answer = answerService.getById(Long.parseLong(answerId));
		
		givenAnswer.setQuiz(quiz);
		givenAnswer.setQuestion(question);
		givenAnswer.setAnswer(answer);
		
		repository.save(givenAnswer);		
	}

	public void update(Long quizId, long questionId, String answerId) {
		GivenAnswer givenAnswer = repository.getOneByQuizIdAndQuestionId(quizId, questionId);
		Answer answer = answerService.getById(Long.parseLong(answerId));
		givenAnswer.setAnswer(answer);
		
		repository.save(givenAnswer);
	}
}
