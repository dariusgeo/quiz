package com.geodani.sda.quiz.respository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.geodani.sda.quiz.model.Category;

@Repository
public interface CategoryRepository  extends JpaRepository<Category, Long> {

}
